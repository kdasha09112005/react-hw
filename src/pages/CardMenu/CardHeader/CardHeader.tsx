import Button from "../../../components/Button/Button";
import { useDispatch, useSelector } from "react-redux";
import { setMealCategory } from "../../../store/slices/menuSlice";
import type { RootState, AppDispatch } from "../../../store/store";

const CardHeader = () => {
    const dispatch = useDispatch<AppDispatch>();
    const mealCategory = useSelector((state: RootState) => state.menu.mealCategory);

    const handleClick = (category: MealCategory) => {
        dispatch(setMealCategory(category));
    };

    return (
        <div className="CardHeader__heading">
            <h1 className="CardHeader__title">Browse our menu</h1>
            <p>Use our menu to place an order online, or <span className="heading__phone">phone<span className="tooltiptext">123-456-7890</span></span> our store to place a pickup order. Fast and fresh food.</p>
            <div className="CardHeader__block-buttons container__items">
                <Button 
                    text="Dessert"
                    size="big"
                    buttonType={mealCategory === 'Dessert' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Dessert')}
                />
                <Button 
                    text="Dinner"
                    size="big"
                    buttonType={mealCategory === 'Dinner' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Dinner')}
                />
                <Button 
                    text="Breakfast"
                    size="big"
                    buttonType={mealCategory  === 'Breakfast' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Breakfast')}
                />
            </div>
        </div>
    );
};

export default CardHeader;
