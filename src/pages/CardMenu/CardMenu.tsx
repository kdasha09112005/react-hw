import React, { useState } from "react";
import Button from "../../components/Button/Button";
import Card from "../../components/Card/Card";
import CardHeader from "./CardHeader/CardHeader";
import { useDispatch, useSelector } from "react-redux";
import type { AppDispatch } from "../../store/store";
import { addToCart } from "../../store/slices/cartSlice";
import { useFetchMealsByCategoryQuery } from '../../store/services/mealsApi';
import { selectFilteredProducts } from "../../store/selectors/menuSelector";
import "./CardMenu.css";

const CardMenu = () => {
  const [itemsToRender, setItemsToRender] = useState(6);
  const dispatch = useDispatch<AppDispatch>();
  const { data: mealsByCategory, isLoading, isError } = useFetchMealsByCategoryQuery();
  const filteredProducts = useSelector(selectFilteredProducts);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error loading meals.</div>;
  }

  if (!mealsByCategory) {
    return <div>No meals available.</div>;
  }

  const productsShown = filteredProducts.slice(0, itemsToRender);

  const loadMore = () => {
    setItemsToRender(prev => prev + 6);
  };

  const handleAddToCart = (id: string, amount: number) => {
    dispatch(addToCart({ id, amount }));
  };

  return (
    <div className="CardMenu__container container__position">
      <div className="CardMenu">
        <CardHeader />
        <div className="CardMenu__cards">
          {productsShown.map((product) => (
            <Card
              key={product.id}
              id={product.id}
              title={product.meal}
              description={product.instructions}
              price={product.price}
              imgSrc={product.img}
              onAddToCart={handleAddToCart}
            />
          ))}
        </div>
        <div className="CardMenu__see-more">
          {itemsToRender < filteredProducts.length && (
            <Button text="See more" size="normal" onClick={loadMore} />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardMenu;

