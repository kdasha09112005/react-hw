import React, { FormEvent, useState } from 'react';
import { auth } from "../../firebase";
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import { usersReceived } from '../../store/slices/usersSlice';
import Button from "../../components/Button/Button";
import "./LoginPage.css";
import { useNavigate } from 'react-router-dom';

const LoginPage = () => {
    const [useremail, setUseremail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const navigate = useNavigate()

    const signIn = (e: FormEvent) => {
        e.preventDefault();
        signInWithEmailAndPassword(auth, useremail, password)
            .then((userCredential) => {
                const user = userCredential.user;
                if (user && user.email) {
                    dispatch(usersReceived({
                        name: user.displayName || '', email: user.email, isLogged: true
                    }));
                    alert("Signed in successfully");
                    navigate("/");
                }
            })
            .catch(() => {
                alert("Username or password is incorrect");
                setUseremail('');
                setPassword('');
            });
    }

    const handleCancel = () => {
        setUseremail('');
        setPassword('');
    };

    return (
        <div className="LoginPage__container container__position">
            <div className='LoginPage__block container__items'>
                <h1>Log in</h1>
                <form className="LoginPage__form container__position" onSubmit={signIn}>
                    <div className="form__block-inputs container__items">
                        <div className="form__item container__items">
                            <label className="form__text">User email</label>
                            <input type="email" placeholder="Enter your email" value={useremail} onChange={(e) => setUseremail(e.target.value)} />
                        </div>
                        <div className="form__item container__items">
                            <label className="form__text">Password</label>
                            <input type="password" placeholder="Enter your password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </div>
                    </div>
                    <div className="form__block-buttons container__items">
                        <Button type="submit" text="Submit" size="primary"/>
                        <Button type="button" text="Cancel" buttonType="secondary" onClick={() => {handleCancel()} }/>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default LoginPage;