import styled from 'styled-components';
import HomeBG from "../../assets/images/HomeBG.svg";

export const HomeContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: url(${HomeBG});
  background-size: cover;
  backgroun-position: right top;

  @media screen and (max-width: 768px) {
    height: 40vh;
  }
`;

export const MainContainer = styled.div`
  display: flex;
  background: url(../../../assets/images/HomeBG.svg)
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 80%;
  height: 100%;

  @media screen and (max-width: 768px) {
    height: 50vh;
  }
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 64%;
  width: 52%;
  align-items: left;

  @media screen and (max-width: 768px) {
    height: 70%;
  }
`;

export const HeadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 60%;
  width: 100%;

  @media screen and (max-width: 768px) {
    height: 66%;
  }
`;

export const Heading = styled.h1`
  font-size: 4vw;
  line-height: 4vw;
  letter-spacing: 1.8px;
  color: var(--card_title-color);

  @media screen and (max-width: 768px) {
    font-size: 3vh;
    line-height: 2.2vh;
  }
`;

export const Subtext = styled.p`
  font-size: 18px;
  font-size: 1.16vw;
  line-height: 1.6vw;
  letter-spacing: 0.36px;

  @media screen and (max-width: 768px) {
    font-size: 1vh;
  }
`;

export const Button = styled.button`
  border: none;
  border-radius: 6px;
  font-size: 1.02vw;
  width: 34%;
  height: 14%;
  cursor: pointer;
  transition: background-color 0.3s ease;
  background-color: var(--primary-color);
  color: var(--card-color);
  border: 2px solid var(--primary-color);

  &:hover {
    background-color: darken(var(--primary-color), 10%);
    border: 2px solid var(--button_secondary-color);
    color: var(--text-color);
  }

  @media screen and (max-width: 768px) {
    font-size: 2vw;
    width: 50%;
    height: 10%;
  }
`;

export const RatingContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between,
  align-items: left;
  width: 100%;
  height: 14%;
`;

export const StarImage = styled.img`
  width: 18%;
  height: 50%;

  @media screen and (max-width: 768px) {
    width: 36%;
    height: 50%;
  }
`;

export const FoodImage = styled.img`
  width: 46%;
  height: 100%;
`;