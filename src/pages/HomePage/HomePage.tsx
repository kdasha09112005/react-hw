import { 
  HomeContainer, MainContainer, InfoContainer, 
  HeadingContainer, Heading, Subtext, 
  Button, RatingContainer, StarImage, 
  FoodImage 
} from './HomePage.styles';
import Star_Logo from "../../assets/images/Logo_Star.svg";
import Food_Image from "../../assets/images/Food_Image.svg";

const HomePage = () => {
  return (
    <HomeContainer>
      <MainContainer>
        <InfoContainer>
          <HeadingContainer>
            <Heading>Beautiful food & takeaway, <span style={{ color: 'var(--primary-color)' }}>delivered</span> to your door.</Heading>
            <Subtext>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</Subtext>
          </HeadingContainer>
          <Button>Place an Order</Button>
          <RatingContainer>
            <StarImage src={Star_Logo} alt="Logo"/>
            <Subtext><span style={{ color: 'var(--primary-color)' }}>4.8 out of 5</span> based on 2000+ reviews</Subtext>
          </RatingContainer>
        </InfoContainer>
        <FoodImage src={Food_Image} alt="Food Delivery" />
      </MainContainer>  
    </HomeContainer>
  );
};
  
export default HomePage;