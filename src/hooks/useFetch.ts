export const useFetch = () => {
  const request = async (
    url: string,
    method: string = "GET",
    body: any = null,
    headers: { [key: string]: string } = { "Content-Type": "application/json" }
  ) => {
    try {
      const response = await fetch(url, { method, body, headers });

      if (!response.ok) {
        throw new Error(`Could not fetch ${url}, status: ${response.status}`);
      }

      const data = await response.json();

      const getApiInfo = localStorage.getItem("ResponseInfo");
      let newInfo = [];

      const currentResponseInfo = {
        status: response.status,
        body: body,
      };

      if (getApiInfo !== null) {
        const parsedInfo = JSON.parse(getApiInfo) as any[];
        newInfo = [...parsedInfo, { currentResponseInfo }];
      } else {
        newInfo = [{ currentResponseInfo }];
      }

      localStorage.setItem("ResponseInfo", JSON.stringify(newInfo));

      return data;
    } catch (e) {
      throw e;
    }
  };
  return { request };
};
