import React from 'react';

type LinkItem = {
  text: string;
  url?: string;
};

type LinksProps = {
  items: LinkItem[];
  className?: string;
  liClassName?: string;
  blockClassName?: string;
  linkClassName?: string;
};

const Links: React.FC<LinksProps> = ({ items, className, liClassName, blockClassName, linkClassName }) => {
  const renderUpperCaseFirstItem = (item: string, index: number) => {
    if (index === 0) {
      return item.toUpperCase();
    }
    return item;
  };

  const renderItem = (item: LinkItem, index: number) => {
    if (index === 0) {
      return <span>{renderUpperCaseFirstItem(item.text, index)}</span>;
    }
    return <a href={item.url} className={linkClassName}>{item.text}</a>;
  };

  return (
    <div className={blockClassName}>
      <ul className={className}>
        {items.map((item, index) => (
          <li key={index} className={liClassName}>
            {renderItem(item, index)}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Links;
