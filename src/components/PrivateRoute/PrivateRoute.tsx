import { Navigate, Outlet } from 'react-router-dom';

interface PrivateRoutesProps {
  hasPermissions: boolean;
  redirectUrlOnFailure: string;
}

export const PrivateRoutes: React.FC<PrivateRoutesProps> = ({ hasPermissions, redirectUrlOnFailure }) => {
  return (
      hasPermissions ? <Outlet /> : <Navigate to={redirectUrlOnFailure} />
  );
};
