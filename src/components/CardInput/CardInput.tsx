import React, { ChangeEvent } from "react";
import "./CardInput.css";

type CardInputProps = {
  value?: number;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
};

const CardInput: React.FC<CardInputProps> = ({ value, onChange }) => {
  return (
    <input
      className="CardInput"
      type="text"
      value={value}
      onChange={onChange}
    />
  );
};

export default CardInput;
