import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectCartItemsCount } from '../../store/selectors/cartSelector';

export const Layout: React.FC = () => {
    const cartItemsCount: number = useSelector(selectCartItemsCount);

    return (
        <div className="wrapper flex-elem">
            <Header productsInCart={cartItemsCount}/>
            <Outlet />
            <Footer />
        </div>
    );
};
