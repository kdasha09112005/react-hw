import React from "react";
import "./Button.css";

type ButtonProps = {
  buttonType?: string;
  size?: string;
  type?: "submit" | "reset" | "button";
  onClick?: () => void;
  text: string;
};

const Button: React.FC<ButtonProps> = ({ buttonType = "primary", size = "small", type = "button", onClick, text }) => {
  return (
    <button
      type={type}
      className={`Button Button--${buttonType} Button--${size}`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
