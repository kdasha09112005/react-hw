import React from "react";
import { ReactComponent as CartIcon } from "../../../assets/images/Busket.svg";
import "./Cart.css";

const Cart: React.FC<{ itemsCount: number }> = ({ itemsCount }) => {
  return (
    <div className="Cart" data-items={itemsCount}>
      <CartIcon />
    </div>
  );
};

export default Cart;
