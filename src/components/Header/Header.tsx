import React from 'react';
import { NavLink } from 'react-router-dom';
import { ReactComponent as Logo } from "../../assets/images/Logo.svg";
import Button from '../Button/Button';
import Cart from "./Cart/Cart";
import { auth } from "../../firebase";
import { signOut } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import type { RootState, AppDispatch } from '../../store/store';
import { clearUser } from '../../store/slices/usersSlice';
import { useSelector } from 'react-redux';
import './Header.css';

const Header: React.FC<{ productsInCart: number }> = ({ productsInCart}) => {
    const dispatch = useDispatch<AppDispatch>();
    const user = useSelector((state: RootState) => state.users.users);

    const handleLogout = () => {               
        signOut(auth).then(() => {
            dispatch(clearUser());
            alert("Signed out successfully")
        }).catch((error) => {
            alert("Something went wrong!")
        });
    }

    return (
        <header className="header container__position">
            <div className="header__container container__items">
                <Logo className="logo"/>
                <div className="header__navigation">
                    <ul className="navigation__list">
                        <li className="list__item">
                            <NavLink to="/" className={({ isActive }) => isActive ? 'active' : undefined}>Home</NavLink>
                        </li>
                        <li className="list__item">
                            <NavLink to="/menu" className={({ isActive }) => isActive ? 'active' : undefined}>Menu</NavLink>
                        </li>
                        <li className="list__item">Company</li>
                        {user.isLogged ? 
                            (<></>) : 
                            (<li className="list__item">
                                <NavLink to="/login" className={({ isActive }) => isActive ? 'active' : undefined}>Login</NavLink>
                            </li> 
                            )
                        }
                    </ul>
                    <Cart itemsCount={productsInCart} />
                    {user.isLogged ? ( <Button text="Sign Out" onClick={handleLogout}/> ) : ( <></> )}
                </div>
            </div>
        </header>
    );
};

export default Header;
