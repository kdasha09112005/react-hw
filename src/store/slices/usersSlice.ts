import { createSlice } from '@reduxjs/toolkit';

interface UserState {
  users: User;
}

const initialState: UserState = {
  users: {
    name: '',
    email: '',
    isLogged: false,
  },
};

const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
      usersReceived(state, action) {
        state.users = action.payload
      },
      clearUser(state) {
        state.users = initialState.users;
      },
    },
})
  

export const { usersReceived, clearUser } = usersSlice.actions;
export default usersSlice.reducer;