import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface CartState {
    cart: Cart;
    cartItemsCount: number;
}

const initialState: CartState = {
    cart: {},
    cartItemsCount: 0,
};

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action: PayloadAction<ProductInCart>) => {
        const { id, amount } = action.payload;
        const existingProduct = state.cart[id];
        if (existingProduct) {
            existingProduct.amount += amount;
        } else {
            state.cart[id] = { amount };
        }
        state.cartItemsCount += amount;
        },
    },
});

export const { addToCart } = cartSlice.actions;
export default cartSlice.reducer;
