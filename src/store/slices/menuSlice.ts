import { createSlice, type PayloadAction } from '@reduxjs/toolkit';

interface MenuState {
    mealCategory: MealCategory | '';
}

const initialState: MenuState = {
    mealCategory: ''
};

const menuSlice = createSlice({
    name: 'menu',
    initialState,
    reducers: {
        setMealCategory(state, action: PayloadAction<MealCategory>) {
        state.mealCategory = action.payload;
        },
    },
});

export const { setMealCategory } = menuSlice.actions;
export default menuSlice.reducer;
