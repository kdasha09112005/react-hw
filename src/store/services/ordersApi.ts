import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

type OrderFromResponse = {
    userId: string;
    meals: ProductInCart[];
    address: {
        street: string;
        building: number;
    };
    id: string;
};

export const ordersApi = createApi({
    reducerPath: 'ordersApi',
    baseQuery: fetchBaseQuery({ baseUrl: "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/" }),
    endpoints: (builder) => ({
        fetchOrders: builder.query<ProductInCart[], string>({
            query: (email) => `?userId=${email}`,
            transformResponse: (response: OrderFromResponse[] | null) => {
                if (!response) {
                    return [];
                }
                console.log(response)
                const modifiedData = response.reduce<ProductInCart[]>((acc, order) => {
                    return acc.concat(order.meals);
                }, []);
                console.log(modifiedData)
                return modifiedData;
            },
        }),
        addOrder: builder.mutation<OrderFromResponse, Partial<OrderFromResponse>>({
            query: (newOrder) => ({
                url: '',
                method: 'POST',
                body: newOrder
            }),
        }),
    }),
});

export const { useFetchOrdersQuery, useAddOrderMutation } = ordersApi;

