import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const mealsApi = createApi({
    reducerPath: 'mealsApi',
    baseQuery: fetchBaseQuery({ baseUrl: "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/meals"}),
    endpoints: (builder) => ({
        fetchMealsByCategory: builder.query<MenuCategorizedData, void>({
            query: () => '',
            transformResponse: (response: Product[]) => {
                const textModifiedData = response.map((product: Product) => {
                    const truncatedText = product.instructions.length > 100 ? `${product.instructions.substring(0, 100)}...` : product.instructions;
                    return { ...product, instructions: truncatedText };
                });

                return textModifiedData.reduce((acc: MenuCategorizedData, meal: Product) => {
                    const category = meal.category;
                    if (!acc[category]) {
                        acc[category] = [];
                    }
                    acc[category]!.push(meal);
                    return acc;
                }, {} as MenuCategorizedData);
            },
        }),
    }),
});


export const { useFetchMealsByCategoryQuery } = mealsApi;