import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './slices/cartSlice';
import menuReducer from './slices/menuSlice';
import usersReducer from './slices/usersSlice';
import { mealsApi } from './services/mealsApi';
import { ordersApi } from './services/ordersApi';

const store = configureStore({
  reducer: {
    cart: cartReducer,
    menu: menuReducer,
    users: usersReducer,
    [mealsApi.reducerPath]: mealsApi.reducer,
    [ordersApi.reducerPath]: ordersApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(mealsApi.middleware, ordersApi.middleware)
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store
