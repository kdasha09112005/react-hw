import { createSelector } from 'reselect';
import type {RootState} from '../store';

const selectCart = (state: RootState) => state.cart.cart;

export const selectCartItemsCount = createSelector(
    [selectCart],
    (cart: Cart) => {
        return Object.values(cart).reduce((prev, cur) => prev + cur.amount, 0);
    }
);
