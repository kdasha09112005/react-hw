import { RootState } from '../store';

export const selectFilteredProducts = (state: RootState) => {
    const mealsByCategory = state.mealsApi.queries['fetchMealsByCategory(undefined)']?.data as MenuCategorizedData | undefined;
    const mealCategory = state.menu.mealCategory;

    if (!mealsByCategory) {
        return [];
    }

    return mealCategory
        ? mealsByCategory[mealCategory] || []
        : Object.values(mealsByCategory).flat();
};
