export const template: { text: string, url?: string }[] = [
  { text: "Template" },
  { url: "https://www.google.com/", text: "Style Guide" },
  { url: "https://www.google.com/", text: "Changelog" },
  { url: "https://www.google.com/", text: "Licence" },
  { url: "https://www.google.com/", text: "Webflow University" },
];
