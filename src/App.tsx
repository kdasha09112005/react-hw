import React, { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import CardMenu from "./pages/CardMenu/CardMenu";
import HomePage from "./pages/HomePage/HomePage";
import LoginPage from "./pages/LoginPage/LoginPage";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";
import { RootState, AppDispatch } from "./store/store";
import { usersReceived } from "./store/slices/usersSlice";

const auth = getAuth();

const App = () => {
  const dispatch = useDispatch<AppDispatch>();
  const cartItemsCount = useSelector((state: RootState) => state.cart.cartItemsCount);
  const user = useSelector((state: RootState) => state.users.users.isLogged);;

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user && user.email) {
        dispatch(usersReceived({
          name: user?.displayName || '',
          email: user.email,
          isLogged: !!user
        }));
      }
    });

    return () => unsubscribe();
  }, [dispatch]);


  return (
    <Router>
      <div className="App">
        <Header productsInCart={cartItemsCount} />
        <div className="App__content container__position">
          <Routes>
            <Route path="/" element={<HomePage />} />
            {user ? (
              <>
                <Route path="/menu" element={<CardMenu />} />
              </>
              ) : (
                <>
                  <Route path="/login" element={<LoginPage />} />
                </>
            )}
            <Route path="/*" element={<Navigate to="/" />} />
          </Routes>
        </div>
        <Footer />
      </div>
    </Router>
  );
};

export default App;

