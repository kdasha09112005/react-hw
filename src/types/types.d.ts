type MealCategory = 'Dessert' |'Dinner' | 'Breakfast';

type ProductInCart = {
    id: string;
    amount: number;
};

interface Amount {
    amount: number;
}

interface Cart {
    [id: string]: Amount
}

type Product = {
    id: string;
    meal: string;
    instructions: string;
    price: string;
    img: string;
    category: MealCategory;
};

type User = {
    name: string;
    email: string;
    isLogged: boolean;
};

type MenuCategorizedData = {
    [K in MealCategory]?: Product[];
};
  
  
  